import React, { useState, useEffect } from 'react';
import { Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { Container, List, Title, Form, Input, Submit } from './styles';
import Repository from '~/components/Repository';

import api from '~/services/api';
import getRealm from '~/services/realm';

export default function Main() {
    const [input, setInput] = useState('')
    const [error, setError] = useState(false)
    const [repositories, setRepositories] = useState([])

    useEffect(() => {
        async function loadRepository() {
            const realm = await getRealm()

            const data = realm.objects('Repository').sorted('stars', true)

            setRepositories(data)
        }

        loadRepository()
    }, [])

    async function saveRepository(rep) {
        const data = {
            id: rep.id,
            name: rep.name,
            fullName: rep.full_name,
            description: rep.description,
            stars: rep.stargazers_count,
            forks: rep.forks_count,
        }

        const realm = await getRealm();

        realm.write(() => {
            realm.create('Repository', data, 'modified')
        })

        return data
    }

    async function handleAddRepository() {
        try {
            const resp = await api.get('/repos/' + input)

            await saveRepository(resp.data)

            setInput('')
            setError(false)
            Keyboard.dismiss()

        } catch (error) {
            setError(true)
            console.tron.warn(error)
        }
    }

    async function handleRefreshRepository(repository) {
        const resp = await api.get('/repos/' + repository.fullName)

        const data = await saveRepository(resp.data)
        setRepositories(repositories.map(r => (r.id == data.id) ? data : r))
    }

    return (
        <Container>
            <Title>Repositorio</Title>

            <Form>
                <Input value={input} error={error} onChangeText={t => setInput(t)} autoCapitalize="none" autoCorrect={false} placeholder="Procurar" />

                <Submit onPress={handleAddRepository}>
                    <Icon name="add" size={22} color="#FFF" />
                </Submit>
            </Form>

            <List kyboardShouldPersistTaps="handled" data={repositories}
                keyExtractor={item => item.id.ttoString()} renderItem={({ item }) => (<Repository onRefresh={() => handleRefreshRepository(item)} data={item} />)} />
        </Container>
    );
}
